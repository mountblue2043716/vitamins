
// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }
const items = require('./3-arrays-vitamins.cjs');
let output = items.reduce((acc, item) => {
    let vitamins = (' '+item.contains).split(',').map((element) => element.slice(1));
    console.log(vitamins);
    vitamins.forEach((vitamin) => {
        if (acc[vitamin] === undefined) {
            acc[vitamin] = [item.name];
        } else {
            acc[vitamin].push(item.name);
        }
    })
    return acc;
},{});
console.log(output);






// let vitamins = ['Vitamin A', 'Vitamin B','Vitamin C','Vitamin D','Vitamin K'];
// let output = items.reduce((acc, item) => {
//     vitamins.forEach((vitamin) => {
//         if (item.contains.includes(vitamin)) {
//             if (acc[vitamin] === undefined) {
//                 acc[vitamin] = [item['name']];
//             } else {
//                acc[vitamin].push(item['name']);
//             }
//         }
//     })
//     return acc;
// }, {});
// console.log(output);
